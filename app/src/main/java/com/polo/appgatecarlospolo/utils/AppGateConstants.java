package com.polo.appgatecarlospolo.utils;

public class AppGateConstants {

    public static final String ERROR_VALIDATE_FIELDS = "Por favor verifique todos los campos antes de continuar";
    public static final String ERROR_USER_ALREADY_EXISTS = "Este usuario ya se encuentra registrado";
    public static final String ERROR_URL = "Por favor verifique la url a la que está intentando acceder";
    public static final String ERROR_COMMUNICATION = "Se presentó un error con el consumo del servicio";
    public static final String URL_ONE = "http://api.geonames.org/timezoneJSON?formatted=true&lat=%s&lng=%s&username=qa_mobile_easy&style=full";

    public static final String TABLE_USER = "user";
    public static final String COLUMN_NAME_USER = "user";
    public static final String COLUMN_NAME_PASS = "pass";
    public static final String TABLE_ATTEMPT = "attempt";
    public static final String COLUMN_NAME_TIME = "time";
    public static final String COLUMN_NAME_STATE = "state";

    public static final String OK = "OK";
    public static final String NO_OK = "Incorrect";
}
