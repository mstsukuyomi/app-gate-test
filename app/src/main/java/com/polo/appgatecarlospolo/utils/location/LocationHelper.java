package com.polo.appgatecarlospolo.utils.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

public class LocationHelper {

    private LocationManager locationManager;

    public LocationHelper(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public LocationResult getCurrentLocation() {
        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastKnownLocation != null) {
            return new LocationResult(
                    lastKnownLocation.getLatitude(),
                    lastKnownLocation.getLongitude()
            );
        }
        return null;
    }
}
