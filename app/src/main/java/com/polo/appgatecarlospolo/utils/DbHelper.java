package com.polo.appgatecarlospolo.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DbHelper extends SQLiteOpenHelper implements BaseColumns {

    private static final String SQL_CREATE_USERS =
            "CREATE TABLE " + AppGateConstants.TABLE_USER + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    AppGateConstants.COLUMN_NAME_USER + " TEXT," +
                    AppGateConstants.COLUMN_NAME_PASS + " TEXT)";

    private static final String SQL_CREATE_ATTEMPTS =
            "CREATE TABLE " + AppGateConstants.TABLE_ATTEMPT + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    AppGateConstants.COLUMN_NAME_USER + " TEXT," +
                    AppGateConstants.COLUMN_NAME_TIME + " TEXT," +
                    AppGateConstants.COLUMN_NAME_STATE + " TEXT)";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DBTest.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USERS);
        db.execSQL(SQL_CREATE_ATTEMPTS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
