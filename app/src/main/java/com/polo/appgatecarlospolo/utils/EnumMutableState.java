package com.polo.appgatecarlospolo.utils;

public enum EnumMutableState {
    SUCCESS, ERROR, INACTIVE
}
