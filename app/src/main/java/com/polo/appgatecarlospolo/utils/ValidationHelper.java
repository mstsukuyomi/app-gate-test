package com.polo.appgatecarlospolo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidationHelper {

    private ValidationHelper() {
    }

    private static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    public static boolean validForm(String user, String pass) {
        user = user.replace(" ", "");
        pass = pass.replace(" ", "");
        if (!user.isEmpty() && !pass.isEmpty() && user.matches(EMAIL_PATTERN)) {
            return validatePass(pass);
        }

        return false;
    }

    private static boolean validatePass(String pass) {
        if(pass.length()>=8) {
            Pattern capital = Pattern.compile("[A-Z]");
            Pattern lowerCase = Pattern.compile("[a-z]");
            Pattern digit = Pattern.compile("[0-9]");
            Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");


            Matcher hasCapital = capital.matcher(pass);
            Matcher hasLowerCase = lowerCase.matcher(pass);
            Matcher hasDigit = digit.matcher(pass);
            Matcher hasSpecial = special.matcher(pass);

            return hasCapital.find() && hasLowerCase.find() && hasDigit.find() && hasSpecial.find();
        } else {
            return false;
        }
    }
}
