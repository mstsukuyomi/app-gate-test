package com.polo.appgatecarlospolo.base;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.polo.appgatecarlospolo.utils.DbHelper;

public class BaseViewModel extends AndroidViewModel {

    protected Context context;
    protected DbHelper dbHelper;

    protected MutableLiveData<String> error = new MutableLiveData<>();

    public MutableLiveData<String> getError() {
        return error;
    }

    public BaseViewModel(@NonNull Application application) {
        super(application);
        this.context = application.getApplicationContext();
        this.dbHelper = new DbHelper(context);
    }
}
