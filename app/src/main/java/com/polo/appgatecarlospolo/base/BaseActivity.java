package com.polo.appgatecarlospolo.base;

import android.widget.Toast;

import com.polo.appgatecarlospolo.di.ViewModelFactory;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    @Inject
    protected ViewModelFactory viewModelFactory;

    private BaseViewModel baseViewModel;

    public void setBaseViewModel(BaseViewModel baseViewModel) {
        this.baseViewModel = baseViewModel;

        this.observeVariables();
    }

    private void observeVariables() {
        baseViewModel.getError().observe(this, error -> {
            Toast.makeText(
                    this,
                    error,
                    Toast.LENGTH_LONG
            ).show();
        });
    }
}
