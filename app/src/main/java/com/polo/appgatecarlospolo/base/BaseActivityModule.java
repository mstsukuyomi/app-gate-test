package com.polo.appgatecarlospolo.base;

import androidx.lifecycle.ViewModel;

import com.polo.appgatecarlospolo.di.ViewModelFactory;

import java.util.Map;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;

@Module
public interface BaseActivityModule {
    @Provides
    static ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }
}
