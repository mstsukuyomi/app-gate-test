package com.polo.appgatecarlospolo.di;

import com.polo.appgatecarlospolo.di.scopes.PerActivity;
import com.polo.appgatecarlospolo.screens.main.MainActivityModule;
import com.polo.appgatecarlospolo.screens.main.view.MainActivity;
import com.polo.appgatecarlospolo.screens.register.RegisterActivityModule;
import com.polo.appgatecarlospolo.screens.register.view.RegisterActivity;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

@Module(includes = AndroidInjectionModule.class)
public interface ActivityBindingsModule {

    @PerActivity
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    MainActivity onMainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = {RegisterActivityModule.class})
    RegisterActivity onRegisterActivityInjector();
}
