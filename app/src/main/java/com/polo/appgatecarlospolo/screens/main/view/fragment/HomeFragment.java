package com.polo.appgatecarlospolo.screens.main.view.fragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polo.appgatecarlospolo.R;
import com.polo.appgatecarlospolo.databinding.FragmentHomeBinding;
import com.polo.appgatecarlospolo.screens.main.view.adapter.AttemptsAdapter;
import com.polo.appgatecarlospolo.screens.main.viewmodel.MainViewModel;

public class HomeFragment extends Fragment {

    protected FragmentHomeBinding binding;
    private MainViewModel viewModel;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        setupBindings();
        return binding.getRoot();
    }

    private void setupBindings() {
        AttemptsAdapter attemptsAdapter = new AttemptsAdapter(
                viewModel.getAttempts()
        );

        binding.rvAttempts.setAdapter(attemptsAdapter);
    }
}
