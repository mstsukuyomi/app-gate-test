package com.polo.appgatecarlospolo.screens.main;

import androidx.lifecycle.ViewModel;

import com.polo.appgatecarlospolo.base.BaseActivityModule;
import com.polo.appgatecarlospolo.di.AppModule;
import com.polo.appgatecarlospolo.screens.main.view.MainActivity;
import com.polo.appgatecarlospolo.screens.main.viewmodel.MainViewModel;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public interface MainActivityModule extends BaseActivityModule {

    @Provides
    @IntoMap
    @AppModule.ViewModelKey(MainViewModel.class)
    static ViewModel viewModel(MainActivity activity) {
        return new MainViewModel(activity.getApplication());
    }
}
