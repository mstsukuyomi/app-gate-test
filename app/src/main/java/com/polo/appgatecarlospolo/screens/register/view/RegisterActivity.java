package com.polo.appgatecarlospolo.screens.register.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.polo.appgatecarlospolo.R;
import com.polo.appgatecarlospolo.base.BaseActivity;
import com.polo.appgatecarlospolo.databinding.ActivityRegisterBinding;
import com.polo.appgatecarlospolo.screens.main.view.MainActivity;
import com.polo.appgatecarlospolo.screens.register.viewmodel.RegisterViewModel;

public class RegisterActivity extends BaseActivity {

    protected ActivityRegisterBinding binding;
    private RegisterViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        viewModel = new ViewModelProvider(this, viewModelFactory).get(RegisterViewModel.class);
        setBaseViewModel(viewModel);

        setupBindings();
        observe();
    }

    private void observe() {
        viewModel.getRegister().observe(this, state -> {
            switch (state) {
                case SUCCESS:
                    Toast.makeText(
                            this,
                            R.string.success_register,
                            Toast.LENGTH_LONG
                    ).show();
                    startActivity(
                            new Intent(this, MainActivity.class)
                    );
                    break;
                case ERROR:
                    Toast.makeText(
                            this,
                            R.string.error_saving_data,
                            Toast.LENGTH_LONG
                    ).show();
                    break;
                default:
                    break;
            }
        });
    }

    private void setupBindings() {
        binding.btnRegister.setOnClickListener(view -> viewModel.register(
                binding.etUser.getText().toString(),
                binding.etPass.getText().toString()
        ));
    }
}
