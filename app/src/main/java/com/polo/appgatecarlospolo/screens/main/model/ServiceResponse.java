package com.polo.appgatecarlospolo.screens.main.model;

import com.google.gson.annotations.SerializedName;

public class ServiceResponse {
    @SerializedName("time")
    private String time;
    @SerializedName("countryName")
    private String countryName;
    @SerializedName("timezoneId")
    private String timezoneId;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = timezoneId;
    }
}
