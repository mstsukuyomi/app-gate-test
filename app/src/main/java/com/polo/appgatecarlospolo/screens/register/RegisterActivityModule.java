package com.polo.appgatecarlospolo.screens.register;

import androidx.lifecycle.ViewModel;

import com.polo.appgatecarlospolo.base.BaseActivityModule;
import com.polo.appgatecarlospolo.di.AppModule;
import com.polo.appgatecarlospolo.screens.register.view.RegisterActivity;
import com.polo.appgatecarlospolo.screens.register.viewmodel.RegisterViewModel;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public interface RegisterActivityModule extends BaseActivityModule {

    @Provides
    @IntoMap
    @AppModule.ViewModelKey(RegisterViewModel.class)
    static ViewModel viewModel(RegisterActivity activity) {
        return new RegisterViewModel(activity.getApplication());
    }
}
