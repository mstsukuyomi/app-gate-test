package com.polo.appgatecarlospolo.screens.main.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.polo.appgatecarlospolo.R;
import com.polo.appgatecarlospolo.databinding.FragmentLoginBinding;
import com.polo.appgatecarlospolo.screens.main.viewmodel.MainViewModel;
import com.polo.appgatecarlospolo.screens.register.view.RegisterActivity;
import com.polo.appgatecarlospolo.utils.EnumMutableState;

import org.jetbrains.annotations.NotNull;

public class LoginFragment extends Fragment {

    protected FragmentLoginBinding binding;
    private MainViewModel viewModel;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        observe();
        setupBindings();
        return binding.getRoot();
    }

    private void observe() {
        viewModel.getLogin().observe(getViewLifecycleOwner(), state -> {
            switch (state) {
                case SUCCESS:
                    Navigation.findNavController(requireView()).navigate(R.id.go_to_homeFragment);
                    viewModel.getLogin().setValue(EnumMutableState.INACTIVE);
                    break;
                case ERROR:
                    Toast.makeText(
                            requireContext(),
                            R.string.error_incorrect_credentials,
                            Toast.LENGTH_LONG
                    ).show();
                    viewModel.getLogin().setValue(EnumMutableState.INACTIVE);
                    break;
                default:
                    break;
            }
        });
    }

    private void setupBindings() {
        binding.btnLogin.setOnClickListener(view -> viewModel.login(
                binding.etUser.getText().toString(),
                binding.etPass.getText().toString()
        ));

        binding.tvRegister.setOnClickListener(view -> startActivity(
                new Intent(requireContext(), RegisterActivity.class)
        ));
    }
}
