package com.polo.appgatecarlospolo.screens.main.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.polo.appgatecarlospolo.R;
import com.polo.appgatecarlospolo.databinding.ItemAttemptBinding;
import com.polo.appgatecarlospolo.screens.main.model.Attempt;

import java.util.List;

public class AttemptsAdapter extends RecyclerView.Adapter<AttemptsAdapter.AttemptsViewHolder> {

    private ItemAttemptBinding binding;
    private List<Attempt> list;

    public AttemptsAdapter(List<Attempt> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public AttemptsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_attempt, parent, false);
        return new AttemptsViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull AttemptsViewHolder holder, int position) {
        Attempt attempt = list.get(position);
        binding.setAttempt(attempt);
    }

    @Override
    public int getItemCount() {
        if (this.list != null) {
            return this.list.size();
        }
        return 0;
    }

    static class AttemptsViewHolder extends RecyclerView.ViewHolder {

        AttemptsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
