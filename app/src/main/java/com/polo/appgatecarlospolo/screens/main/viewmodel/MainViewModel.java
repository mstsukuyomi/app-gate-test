package com.polo.appgatecarlospolo.screens.main.viewmodel;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.polo.appgatecarlospolo.base.BaseViewModel;
import com.polo.appgatecarlospolo.screens.main.model.Attempt;
import com.polo.appgatecarlospolo.screens.main.model.ServiceResponse;
import com.polo.appgatecarlospolo.utils.AppGateConstants;
import com.polo.appgatecarlospolo.utils.EnumMutableState;
import com.polo.appgatecarlospolo.utils.ValidationHelper;
import com.polo.appgatecarlospolo.utils.location.LocationHelper;
import com.polo.appgatecarlospolo.utils.location.LocationResult;
import com.polo.communication.CommunicationCallback;
import com.polo.communication.CommunicationManager;
import com.polo.communication.EnumRequestType;
import com.polo.communication.RequestObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends BaseViewModel implements CommunicationCallback<ServiceResponse> {

    private static final String TAG = MainViewModel.class.getSimpleName();

    private MutableLiveData<EnumMutableState> login = new MutableLiveData<>();

    private String user;
    private String pass;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<EnumMutableState> getLogin() {
        return login;
    }

    public void login(String user, String pass) {
        if (ValidationHelper.validForm(user, pass)) {
            this.user = user;
            this.pass = pass;
            LocationHelper locationHelper = new LocationHelper(this.context);
            LocationResult currentLocation = locationHelper.getCurrentLocation();

            String url = createUrl(currentLocation);

            try {
                RequestObject requestObject = new RequestObject();
                requestObject.setRequestType(EnumRequestType.GET);

                CommunicationManager communicationManager = new CommunicationManager(url, ServiceResponse.class, this);
                communicationManager.execute(url);
            } catch (MalformedURLException e) {
                Log.e(TAG, e.getMessage(), e);
                error.setValue(AppGateConstants.ERROR_URL);
            }
        } else {
            error.setValue(AppGateConstants.ERROR_VALIDATE_FIELDS);
        }
    }

    private String createUrl(LocationResult currentLocation) {
        return String.format(AppGateConstants.URL_ONE, currentLocation.getLatitude(), currentLocation.getLongitude());
    }

    @Override
    public void success(ServiceResponse response) {
        String validateResponse = validateUser();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppGateConstants.COLUMN_NAME_USER, this.user);
        values.put(AppGateConstants.COLUMN_NAME_TIME, response.getTime());
        values.put(AppGateConstants.COLUMN_NAME_STATE, validateResponse);

        long newRowId = db.insert(AppGateConstants.TABLE_ATTEMPT, null, values);

        Log.i("Rows modified", String.valueOf(newRowId));

        if (AppGateConstants.OK.equals(validateResponse)) {
            login.postValue(EnumMutableState.SUCCESS);
        } else {
            login.postValue(EnumMutableState.ERROR);
        }
    }

    private String validateUser() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                AppGateConstants.COLUMN_NAME_USER,
                AppGateConstants.COLUMN_NAME_PASS
        };

        String selection = AppGateConstants.COLUMN_NAME_USER + " = ? AND " + AppGateConstants.COLUMN_NAME_PASS + " = ?";
        String[] selectionArgs = {this.user, this.pass};

        String sortOrder =
                AppGateConstants.COLUMN_NAME_USER + " DESC";

        Cursor cursor = db.query(
                AppGateConstants.TABLE_USER,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        List itemIds = new ArrayList<>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(BaseColumns._ID));
            itemIds.add(itemId);
        }
        cursor.close();

        if (itemIds.isEmpty()) {
            return AppGateConstants.NO_OK;
        } else {
            return AppGateConstants.OK;
        }
    }

    @Override
    public void error(String error) {
        Log.e(TAG, error);
        this.error.setValue(AppGateConstants.ERROR_COMMUNICATION);
    }

    public List<Attempt> getAttempts() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                AppGateConstants.COLUMN_NAME_USER,
                AppGateConstants.COLUMN_NAME_TIME,
                AppGateConstants.COLUMN_NAME_STATE
        };

        String selection = AppGateConstants.COLUMN_NAME_USER + " = ?";
        String[] selectionArgs = {this.user};

        String sortOrder =
                AppGateConstants.COLUMN_NAME_TIME + " DESC";

        Cursor cursor = db.query(
                AppGateConstants.TABLE_ATTEMPT,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        List<Attempt> items = new ArrayList<>();
        while (cursor.moveToNext()) {
            items.add(
                    new Attempt(
                            cursor.getString(cursor.getColumnIndexOrThrow(AppGateConstants.COLUMN_NAME_TIME)),
                            cursor.getString(cursor.getColumnIndexOrThrow(AppGateConstants.COLUMN_NAME_STATE))
                    )
            );
        }
        cursor.close();

        return items;
    }
}
