package com.polo.appgatecarlospolo.screens.main.model;

public class Attempt {

    private String time;
    private String state;

    public Attempt(String time, String state) {
        this.time = time;
        this.state = state;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
