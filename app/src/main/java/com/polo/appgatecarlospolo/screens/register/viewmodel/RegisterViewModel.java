package com.polo.appgatecarlospolo.screens.register.viewmodel;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.polo.appgatecarlospolo.base.BaseViewModel;
import com.polo.appgatecarlospolo.utils.AppGateConstants;
import com.polo.appgatecarlospolo.utils.EnumMutableState;
import com.polo.appgatecarlospolo.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public class RegisterViewModel extends BaseViewModel {

    private MutableLiveData<EnumMutableState> register = new MutableLiveData<>();

    public RegisterViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<EnumMutableState> getRegister() {
        return register;
    }

    public void register(String user, String pass) {
        if (ValidationHelper.validForm(user, pass)){
            if (AppGateConstants.OK.equals(this.validateUser(user))) {
                this.registerUser(user, pass);
            } else {
                error.setValue(AppGateConstants.ERROR_USER_ALREADY_EXISTS);
            }
        } else {
            error.setValue(AppGateConstants.ERROR_VALIDATE_FIELDS);
        }
    }

    private void registerUser(String user, String pass) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AppGateConstants.COLUMN_NAME_USER, user);
        values.put(AppGateConstants.COLUMN_NAME_PASS, pass);

        long newRowId = db.insert(AppGateConstants.TABLE_USER, null, values);

        if (newRowId > 0) {
            register.setValue(EnumMutableState.SUCCESS);
        } else {
            register.setValue(EnumMutableState.ERROR);
        }
    }

    private String validateUser(String user) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                AppGateConstants.COLUMN_NAME_USER,
                AppGateConstants.COLUMN_NAME_PASS
        };

        String selection = AppGateConstants.COLUMN_NAME_USER + " = ?";
        String[] selectionArgs = { user };

        String sortOrder =
                AppGateConstants.COLUMN_NAME_USER + " DESC";

        Cursor cursor = db.query(
                AppGateConstants.TABLE_USER,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        List itemIds = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(BaseColumns._ID));
            itemIds.add(itemId);
        }
        cursor.close();

        if (itemIds.isEmpty()) {
            return AppGateConstants.OK;
        } else {
            return AppGateConstants.NO_OK;
        }
    }
}
