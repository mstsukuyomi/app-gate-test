package com.polo.communication;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CommunicationManager extends AsyncTask<String, Void, String> {

    private static final String TAG = CommunicationManager.class.getSimpleName();

    private HttpURLConnection client;
    private Class<?> response;
    private CommunicationCallback callback;

    public CommunicationManager(
            String stringUrl,
            final Class<?> responseClass,
            CommunicationCallback callback
    ) throws MalformedURLException {
        response = responseClass;
        this.callback = callback;

        URL url = new URL(stringUrl);
        try {
            this.client = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            int responseCode = this.client.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String serverResponse = readStream(this.client.getInputStream());

                Log.v("RESPONSE###", serverResponse);
                this.callback.success(new Gson().fromJson(serverResponse, response));
            } else {
                this.callback.error("HTTP different to 200");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage(), e);
            this.callback.error(e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            this.callback.error(e.getMessage());
        }

        return null;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            this.callback.error(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                    this.callback.error(e.getMessage());
                }
            }
        }
        return response.toString();
    }
}
