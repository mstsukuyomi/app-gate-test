package com.polo.communication;

import java.util.Map;

public class RequestObject {

    private EnumRequestType requestType;
    private Map<String, String> body;

    public EnumRequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(EnumRequestType requestType) {
        this.requestType = requestType;
    }

    public Map<String, String> getBody() {
        return body;
    }

    public void setBody(Map<String, String> body) {
        this.body = body;
    }
}
