package com.polo.communication;

public interface CommunicationCallback<T> {

    void success(T response);

    void error(String error);
}
