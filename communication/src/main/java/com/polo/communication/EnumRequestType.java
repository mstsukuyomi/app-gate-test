package com.polo.communication;

public enum EnumRequestType {
    POST("POST"), GET("GET");

    private String type;

    EnumRequestType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
